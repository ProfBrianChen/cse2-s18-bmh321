public class WelcomeClass {
  
  public static void main(String [] args){
    //
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-B--M--H--3--2--1->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("v  v  v  v  v  v");
    System.out.println("I am Brock Herring, I enjoy wrestling and chess, my favorite food is Roast beef and Au Juis, and my favorite subject is math");
  }
  
}