//lab 10
import java.util.Scanner;

public class Multi{
  public static int[][] increasingMatrix(int w, int h, boolean format){
    int[][] x = new int[w][h];
    int[][] v = new int[w][h];
    int a = 0;
    if(format == true){
      for(int i = 0; i < x.length; i++){
        for(int j = 0; j < x[i].length; j++){
           x[i][j] = (int)((Math.random() * 9) + 1);
           System.out.print( x[i][j] + " ");
        }// for loop
        System.out.println(" ");
      }// for loop
      
      return x;
    }// if statement
    else{
      for(int k = 0; k < v.length; k++){
        System.out.print("[");
        for(int l = 0; l < v[k].length; l++){
          v[k][l] = a;
          a++;
          System.out.print( v[k][l] + " ");
        }// for loop
        System.out.println("]");
      }// for loop
      return v;
    }// else statement
  }// end method
  
  public static void printMatrix(int[][] array, boolean b){
    
    if( array == null){
      System.out.println("Array was not valid!");
    }// end of if statement
    
    if( b == true){
      for(int m = 0; m < array.length; m++){
        for(int n = 0; n < array[m].length; n++){
          System.out.print(array[n][m] + " "); 
        }
        System.out.println(" ");
      }// end of for loops
    }// end of if statement
    else{
      for(int o = 0; o < array.length; o++){
        for(int p = 0; p < array[o].length; p++){
          System.out.print(array[o][p] + " "); 
        }
        System.out.println(" ");
      }// end of for loops
   }// end of else statement
  }// end of method 
  
  public static int[][] translate(int[][] c){
    int[][] temp = new int[c.length][c[0].length];
    
    for(int q = 0; q < c.length; q++){
      for(int r = 0; r < c[q].length; r++){
        
        temp[q][r] = c[r][q];
        c[r][q] = c[q][r];
        c[q][r] = temp[q][r];
        
      }
      
      
    }// end for loop
    return c;
  }// end of method
  
  public static int[][] addMatrix(int[][] f, boolean jelly, int[][] g, boolean jam){
    if(f.length != g.length || f[0].length != g[0].length){
      System.out.println("These cannot be added!");
      return null;
    }// end of if statement
    
    int[][] h = new int[f.length][f[0].length];
    
    if(jelly == false){
      translate(f);
    }// end of if statement
    
    if(jam == false){
      translate(g);
    }// end of if statement
    
    for(int s = 0; s < f.length; s++){
       for(int t = 0; t < f[s].length; t++){
         h[s][t] = (f[s][t] + g[s][t]);
       }
    }//end of for loop
    
    return h;
  }// end of method
  
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    System.out.print("Give a y: ");
    int y = scan.nextInt();
    System.out.print("Give an x: ");
    int z = scan.nextInt();
    
    int[][] a = increasingMatrix(y, y, true);
    int[][] b = increasingMatrix(y, y, false);
    int[][] c = increasingMatrix(z, z, true);
    
    printMatrix(a, true);
    printMatrix(b, false);
    printMatrix(c, true);
    
    int[][] d = addMatrix(a, true, b, false);
    int[][] e = addMatrix(a, true, c, true);
    
    printMatrix(d, true);
    printMatrix(e, true);
  }// end method
}// end class