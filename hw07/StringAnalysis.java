import java.util.Scanner;

public class StringAnalysis{
  
  public static void main (String[] args){ // main method
    Scanner myScanner1 = new Scanner ( System.in); // declares scanner
    System.out.print("Enter your string variable: ");
    String b = myScanner1.next(); 
    System.out.println("Enter 1 if you want to analyze the entire string and 2 if you want to enter a certain number of characters to analyze: ");//prompts user for whether to look at whole string or each character
    int userDecision = myScanner1.nextInt(); 
    if (userDecision == 1){
      stringAnalysis (b); // calls method
    }
    else if (userDecision == 2 ){
      System.out.print ("Enter your string variable again: "); // sets up string for method
      String y = myScanner1.next();
      System.out.println("Enter the number of characters you want to analyze in the string: "); // prompts user for how many characters they want analyzed
      int lengthEvaluation = myScanner1.nextInt();
      stringAnalysis (y, lengthEvaluation); // calls method 
    }//end of if
  }// end of main method
  
  public static boolean stringAnalysis (String a){
    int i = 0;
    int characterCount = a.length(); 
    if (Character.isLetter(a.charAt(i))){
      if (i <= characterCount){ // checks throughout string
        i++;
        System.out.println("All characters in the string are letters.");
      }// end of if
       
    }
    else {
        System.out.println("Not all of the characters in the string are letters.");
      
  }//end of if 
    return true;
}//end of method
  
  public static boolean stringAnalysis (String e, int lengthEvaluation){
    int j = 0;
    int characterCount1 = e.length();
    if (j >= lengthEvaluation){
      if (Character.isLetter(e.charAt(j))){ // checks throughout string
        j++;     
        System.out.println("All characters in selected length are letters.");
      } // end of if
    } // end of if
    return true;
  }// end of method
    
}// end of class


