import java.util.Scanner;

public class Area {    
    public static String checkInput (){ //This little method checks for input
    
      Scanner scan = new Scanner(System.in);
      System.out.println("Enter: rectangle, triangle or circle (no capitolization!)");//shapes to choose from
      String shape = scan.next(); //inputed shape gets stored in String type
      boolean check = true;
      while(check){ //will run until broken, so question will keep being asked
        if (shape.equals("rectangle")){
          break;
        }
        else if (shape.equals("triangle")){
          break;
        }
        else if (shape.equals("circle")){
          break;
        }
        else {
            System.out.println("Wrong input, try again. Enter: rectangle, triangle or circle (no capitolization!)");//prompts user to input correct shape
            shape = scan.next();
        }  
       }//while loop 
        return shape;
     }//method 
 
  public static double rectangle (){ // completes area for a rectangle
    Scanner scan = new Scanner(System.in);
    System.out.println("length: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }// while loop
    double length = scan.nextDouble();
    
    System.out.println("width: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }// while loop
    double width = scan.nextDouble();
    
    double area = length * width;
    return area;
  }//rectangle method ends
  
  
   public static double triangle (){ // completes area for a triangle
    Scanner scan = new Scanner(System.in);
    System.out.println("length: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }// while loop
    double length = scan.nextDouble();
    
    System.out.println("base: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    } // while loop
    double base = scan.nextDouble();
    
    double area = (length * base)/2;
    return area;
  }//trinagle method ends
  
  
   public static double circle (){ // completes the area of a circle
    Scanner scan = new Scanner(System.in);
    System.out.println("radius: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    } // while loop
    double radius = scan.nextDouble();
    
    double area = Math.pow(radius,2)*3.14;
    return area;
  }//cirlce method ends
  
   public static void main(String [] args){
     
    String shape = checkInput() ;//shape that will be used 
    double area = 0;
    if (shape.equals("rectangle")){
          area = rectangle();//assigns value stored in rectangle method to area
        }
        else if (shape.equals("triangle")){
          area = triangle();//assigns value stored in triangle method to area
        }
        else if (shape.equals("circle")){
          area = circle();//assigns value stored in circle method to area
        } // if statement
    System.out.println("Area of " + shape + " is: " + area );
  }//main method ends
  
}//class
