// Your program is to write loops that asks the user to enter information relating to a course they are currently taking
// - ask for the course number, department name, the number of times it meets in a week, 
//the time the class starts, the instructor name, and the number of students.  
//For each item, check to make sure that what the user enters is actually of the correct type, 
//and if not, use an infinite loop to ask again.
import java.util.Scanner;

public class Hw05 {
  public static void main(String[] args){
    Scanner nameScan = new Scanner( System.in ); // set up mulitple scanners to fix potential bug, unnessecary, yet helpful
    Scanner depScan = new Scanner( System.in );
    Scanner teachScan = new Scanner( System.in );
    Scanner courseScan = new Scanner( System.in );
    Scanner manyScan = new Scanner( System.in );
    Scanner startScan = new Scanner( System.in );
    Scanner endScan = new Scanner( System.in );
    Scanner studentScan = new Scanner( System.in );
    
    String yourName = ""; // these are the string values we require from the user
    String department = "";
    String teachName = "";
    
    int courseNum = 0; // these are the integer values we require from the user
    int howMany = 0;
    int classStart = 0;
    int classEnd = 0;
    int numStudents = 0;
    
    int a = 0; // these ints help conduct the while loops, using counters
    int b = 0;
    int c = 0;
    int d = 0;
    int e = 0;
    int f = 0;
    int g = 0;
    int h = 0;
    
    while (a < 1){ // tests while loop condition(s)
      System.out.print("What's your name?"); // asks user their name
      if(nameScan.hasNextLine() == true){ // tests to see if user submitted a string
        yourName = yourName.concat(nameScan.nextLine()); // assigns variable 
        a++; // counter for while loop 
      }
      else {
        String junkWord = nameScan.next(); // discards of unwanted responses
      } // end of if/else statement
    } // end of while loop
      
    
    while(b < 1){ // tests while loop condition(s)
      System.out.print("What's the department name?"); // asks user department name
      if(depScan.hasNextLine() == true){ // tests to see if user submitted a string
        department = department.concat(depScan.nextLine()); // assigns variable 
        b++; // counter for while loop 
      }
      else {
        String junkWord = depScan.next(); // discards of unwanted responses
      } // end of if/else statement
    } // end of while loop
      
    while(c < 1){ // tests while loop condition(s)
      System.out.print("What's the professors name?"); // asks user professors name
      if(teachScan.hasNextLine() == true){ // tests to see if user submitted a string
        teachName = teachName.concat(teachScan.nextLine()); // assigns variable 
        c++; // counter for while loop 
      }
      else {
        String junkWord = teachScan.next(); // discards of unwanted responses
      } // end of if/else statement
    } // end of while loop
      
    while(d < 1 || courseNum <= 0){ // tests while loop condition(s)
      System.out.print("What's the course number?"); // asks user for course number
      if(courseScan.hasNextInt() == true){ // tests to see if user submitted an int
        courseNum = courseScan.nextInt(); // assigns variable 
        d++; // counter for while loop 
      }
      else {
        String junkWord = courseScan.next(); // discards of unwanted responses
      } // end of if/else statement
    } // end of while loop
      
    while(e < 1 || 24 < classStart || classStart < 0){ // tests while loop condition(s)
      System.out.print("What's the start time (In Military) ?"); // asks user when the class starts 
      if(startScan.hasNextInt() == true){ // tests to see if user submitted an int
        classStart = startScan.nextInt(); // assigns variable 
        e++; // counter for while loop 
      }
      else {
        String junkWord = startScan.next(); // discards of unwanted responses
      } // end of if/else statement
    } // end of while loop
   
    while(f < 1 || 24 < classEnd || classEnd < 0){ // tests while loop condition(s)
      System.out.print("What's the end time (In Military) ?"); // asks user when the class ends
      if(endScan.hasNextInt() == true){ // tests to see if user submitted an int
        classEnd = endScan.nextInt(); // assigns variable 
        f++; // counter for while loop 
      }
      else {
        String junkWord = endScan.next(); // discards of unwanted responses
      } // end of if/else statement
    } // end of while loop
      
    while(g < 1 || howMany <= 0){ // tests while loop condition(s)
      System.out.print("How many times does the class meet per week?"); // asks user how many times the class will meet per week
      if(manyScan.hasNextInt() == true){ // tests to see if user submitted an int
        howMany = manyScan.nextInt(); // assigns variable 
        g++; // counter for while loop 
      }
      else {
        String junkWord = manyScan.next(); // discards of unwanted responses
      } // end of if/else statement
    } // end of while loop
      
    while(h < 1 || numStudents <= 0){ // tests while loop condition(s)
      System.out.print("How many students are in the class?"); // asks user how many students are in class
      if(studentScan.hasNextInt() == true){ // tests to see if user submitted an int
        numStudents = studentScan.nextInt(); // assigns variable 
        h++; // counter for while loop 
      }
      else {
        String junkWord = studentScan.next(); // discards of unwanted responses
      } // end of if/else statement
    } // end of while loop
    
    System.out.println("You, " + yourName + ", have " + department + " " + courseNum + " from " + classStart + " to " + classEnd + " for " + howMany + " times a week, it is taught by " + teachName + " and there are " + numStudents + " students in the class.");
    // prints to screen
  } // end of method
} // end of class