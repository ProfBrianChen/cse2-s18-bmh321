//The purpose of this lab is to get familiar with loops, 
//a critical piece of syntax that is essential for many programming languages.  
//The program you will write will print out a simple twist on the screen.

import java.util.Scanner;

public class TwistGenerator {
  public static void main(String[] args){
    int length = 0;
    int a = 0;
    int b = 0;
    int c = 0;
    Scanner scan = new Scanner(System.in);
    
    while (length < 1){
      System.out.print("Give us a positive integer for the length");
      if(scan.hasNextInt() == true){
        length = scan.nextInt();
      }
      else {
        String junkWord = scan.next();
      }
    }
    
    int rem = length % 3;
    
    String topLayerOne = "\\" ;
    String topLayerTwo = " " ;
    String topLayerThree = "/" ;
    
    String midLayerOne = " ";
    String midLayerTwo = "X";
    String midLayerThree = " ";
    
    String botLayerOne = "/";
    String botLayerTwo = " ";
    String botLayerThree = "\\";
    
    String topSeg = "";
    while ( a < length){
      topSeg = topSeg.concat(topLayerOne);
      a++;
      if (a == length){
        break;
      }
      topSeg = topSeg.concat(topLayerTwo);
      a++;
      if (a == length){
        break;
      }
      topSeg = topSeg.concat(topLayerThree);
      a++;
      if (a == length){
        break;
      }
    }
    String midSeg = "";
    while ( b < length){
      midSeg = midSeg.concat(midLayerOne);
      b++;
      if (b == length){
        break;
      }
      midSeg = midSeg.concat(midLayerTwo);
      b++;
      if (b == length){
        break;
      }
      midSeg = midSeg.concat(midLayerThree);
      b++;
      if (b == length){
        break;
      }
    }
    String botSeg = "";
    while ( c < length){
      botSeg = botSeg.concat(botLayerOne);
      c++;
      if (c == length){
        break;
      }
      botSeg = botSeg.concat(botLayerTwo);
      c++;
      if (c == length){
        break;
      }
      botSeg = botSeg.concat(botLayerThree);
      c++;
      if (c == length){
        break;
      }
    }
      
    System.out.println( topSeg );
    System.out.println( midSeg );
    System.out.println( botSeg );
    
  } // end of method
} // end of class