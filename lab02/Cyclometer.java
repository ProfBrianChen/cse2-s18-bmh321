// Brock Herring, Feb. 2, 2018, lab 02 -> Cyclometer
// Cyclometer will measure time traveled and rotations of front wheel
public class Cyclometer{
  public static void main(String[] args){
  // main class for calling functions within Cyclometer
  
  
  int secsTrip1=480;  // time in seconds for trip 1
  int secsTrip2=3220;  // time in seconds for trip 2
	int countsTrip1=1561;  // rotations of front wheel for trip 1
	int countsTrip2=9037; // rotations of front wheel for trip 2
  
  double wheelDiameter=27.0,  // Diameter of our rotating wheel
   PI=3.14159, // constant value for PI
   feetPerMile=5280,  // conversion from feet to miles
   inchesPerFoot=12,   // conversion from inches to feet
   secondsPerMinute=60;  // conversion from seconds to minutes
	double distanceTrip1, distanceTrip2, totalDistance;  // establishes variables for these, which will be computed later

  distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Gives distance in miles
  totalDistance=distanceTrip2+distanceTrip1; // Gives total Distance
  
  
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");
 
  }
}