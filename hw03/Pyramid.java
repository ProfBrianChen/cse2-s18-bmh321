import java.util.Scanner;
// so that we can take information from the user
public class Pyramid{
// essential for all java methods
  public static void main(String[] args){
  // where the magic happens
  Scanner myScanner = new Scanner( System.in ); // declares scanner
  System.out.print("What is the length of the square of the pyramid? "); // asks user for length of pyramid's base
  double sideLength = myScanner.nextDouble();
  System.out.print("What is the height of the pyramid? "); // asks user for height of pyramid
  double pyramidHeight = myScanner.nextDouble();
  // ((base ^ 2) * h)/3 = volume of pyramid
  double baseArea = Math.pow( sideLength, 2.0 ); // finds the area of the pyramid's base
  double pyramidVolume = ((baseArea * pyramidHeight ) / 3); // finds volume of the pyramid
  System.out.println("The volume of the pyramid is: " + pyramidVolume); //prints answer to screen for the user
  
  } // end of main method

} // end of class