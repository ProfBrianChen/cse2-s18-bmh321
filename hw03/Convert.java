import java.util.Scanner;
// so that we can take info from the user

public class Convert{
// our main class
  public static void main(String[] args){
  Scanner myScanner = new Scanner( System.in ); // declares scanner
  System.out.print("Enter the amount of affected land in acres: "); // prompts user for amount of affected area in acres
  double affectedAcres = myScanner.nextDouble();
  System.out.print("Enter the amount of average rainfall in inches: "); // prompts user for amount of average rainfall in inches
  double averageInches = myScanner.nextDouble(); 
    
  double squareMiles = 0.0015625; // conversions 
  double inchesInAMile = 63360; // conversions
    
  double areaAffected = (affectedAcres * squareMiles); // base area of the rainfall in miles
  double rainFall = (averageInches / inchesInAMile); // height of the rainfall in miles
  
  double squareMilesRain = areaAffected * rainFall; // final answer 
  System.out.println("The amount of average rainfall in cubic miles was: " + squareMilesRain); //prints out our final answer for the user
  } //end of main 
  
} //end of class
