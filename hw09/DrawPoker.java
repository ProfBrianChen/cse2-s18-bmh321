import java.util.Arrays; 

public class DrawPoker{
  
  public static void main (String[] args){
    
    int [] deck = new int [52]; //creating array for the deck of cards
    
    for (int i = 0; i < deck.length; i++){
      deck [i] = i; 
    } //for loop to initialize every index of the array
    
    
    //shuffling the deck 
    for (int i = 0; i < deck.length; i++){
      
      int index = (int) (Math.random() * 52); 
      int temp = deck [i]; 
      deck [i] = deck [index];
      deck [index] = temp;
      
    } //for loop 
    
    
    //creating an array for each player's hand 
    
    int [] player1 = new int [5];
    int [] player2 = new int [5];
    
    //initializing all elements for each player (giving each player their hand of 5 cards)
    
    player1[0] = deck[0];
    player1[1] = deck[2];
    player1[2] = deck[4];
    player1[3] = deck[6];
    player1[4] = deck[8];
    
    player2[0] = deck[1];
    player2[1] = deck[3];
    player2[2] = deck[5];
    player2[3] = deck[7];
    player2[4] = deck[9];
   
    //printing out each player's hand 
    
    System.out.print("Player 1's hand: "); 
    for (int i = 0; i < ranks(player1).length; i++){
      System.out.print(ranks(player1)[i] + " ");
    } //printing out player one's hand
          System.out.println(" ");
    
    System.out.print("Player 2's hand: ");
    for (int i = 0; i < ranks(player2).length; i++){
      System.out.print(ranks(player2)[i] + " ");
    } //for loop to printing out player two's hand 
          System.out.println(" ");
    
    //printing out the results of each players hand, using the methods to determine whether they are true or false 
    //checking for pairs, three of a kind, flush, full house 
    
    System.out.println("Does player 1 have any pairs: " + pair(player1));
    System.out.println("Does player 2 have any pairs: " + pair(player2));
    
    System.out.println("Does player 1 have three of a kind: " + threeOfAKind(player1));
    System.out.println("Does player 2 have three of a kind: " + threeOfAKind(player2));

    System.out.println("Does player 1 have a flush: " + flush(player1));
    System.out.println("Does player 2 have a flush: " + flush(player2));
    
        
    System.out.println("Does player 1 have a full house: " + fullhouse(player1));
    System.out.println("Does player 2 have a full house: " + fullhouse(player2));
    
    
    //determining which player has a highesr ranking card 
    
    int highestRankPlayer1 = highestRank(player1); 
    int highestRankPlayer2 = highestRank(player2); 
    if (highestRankPlayer1 > highestRankPlayer2){
      System.out.println("Player 1 has the highest ranking card.");
    } // if statement 
    else if (highestRankPlayer2 > highestRankPlayer1){
      System.out.println("Player 2 has the highest ranking card.");
    } //else if  statement
    else {
      System.out.println("Both players have the same highest ranking card.");
    }
    
    //deciding who the winner of the game is 
    
    String winner = ""; 
    
    if (fullhouse(player1) != fullhouse(player2)){
      if (fullhouse(player1) == true){
      winner = "Player 1"; 
    }
    else if (fullhouse(player2) == true){
      winner = "Player 2";
    }
    }//highest hand : full house
    
    if (flush(player1) != flush(player2)){
      if (flush(player1) == true){
      winner = "Player 1"; 
    }
    else if (flush(player2) == true){
      winner = "Player 2";
    }
    } //second highest hand: flush 
    
    if (threeOfAKind(player1) != threeOfAKind(player2)){
      if (threeOfAKind(player1) == true){
      winner = "Player 1"; 
    }
    else if (threeOfAKind(player2) == true){
      winner = "Player 2";
    }
    } //third highest hand: three of a kind
    
    
    if (pair(player1) != pair(player2)){
      if (pair(player1) == true){
      winner = "Player 1"; 
    }
    else if (pair(player2) == true){
      winner = "Player 2";
    }
    }
    else{
      
        if (highestRankPlayer1 > highestRankPlayer2){
      winner = "Player 1";
    }// if statement 
      else if (highestRankPlayer2 > highestRankPlayer1){
      winner = "Player 2";  
    } //else if  statement
      else {
      winner = "There is no winner, they have equal hands";
    }
      } //fourth highest hand: pair, and if not, highest ranking card wins 
    
    //printing out the winner of the game
    
    System.out.println("The winner of this game is: " + winner); 
    
  } //main method 
    
    public static boolean pair (int [] hand){
      
      boolean chowder;
      int count = 0; 
      
      if  (hand[0] % 13 == hand[1] % 13 ||
           hand[0] % 13 == hand[2] % 13 ||
           hand[0] % 13 == hand[3] % 13 ||
           hand[0] % 13 == hand[4] % 13 ||
           hand[1] % 13 == hand[2] % 13 ||
           hand[1] % 13 == hand[3] % 13 || 
           hand[1] % 13 == hand[4] % 13 ||
           hand[2] % 13 == hand[3] % 13 ||
           hand[2] % 13 == hand[4] % 13 || 
           hand[3] % 13 == hand[4] % 13
          ){
        count++;  
      } //if statement 
        
      if (count > 0){
        chowder = true; 
      } //if statement 
      else {
        chowder = false; 
      } //else statement
      
      return chowder; 
      
    } //pair method 
  
  
  public static boolean threeOfAKind (int [] hand){
    
    boolean gumbo;
    int count = 0;
    int x = 2; 
    int Val1 = hand[0] %13;
    int Val2 = hand[1] %13; 
    
    for (int Val3 = hand[x] % 13; x < hand.length; x++){
      if (Val1 == Val2 && Val2 == Val3){
        count++;
      }// if statement
    }// for loop
    
    Val1 = hand[1] % 13; 
    Val2 = hand[2] % 13;
    x =3; 
    
    for (int Val3 = hand [x] % 13; x < hand.length; x++){
      if (Val1 == Val2 && Val2 == Val3){
        count++;
      }// if statement
    }// for loop
    
    Val1 = hand[2] % 13 ; 
    Val2 = hand[3] % 13;
    int Val3 = hand[4] % 13;
    
    if (Val1 == Val2 && Val2 == Val3){
      count++;
    }// if statement
    
    Val1 = hand[3] % 13; 
    Val2 = hand[0] % 13; 
    Val3 = hand[2] % 13; 
    
    if (Val1 == Val2 && Val2 == Val3){
      count++;
    }// if statement
    
    Val1 = hand[3] % 13; 
    Val2 = hand[0] % 13; 
    Val3 = hand[4] % 13; 
    
    if (Val1 == Val2 && Val2 == Val3){
      count++;
    }// if statement
    
    Val1 = hand[3] % 13; 
    Val2 = hand[1] % 13; 
    Val3 = hand[4] % 13; 
    
    if (Val1 == Val2 && Val2 == Val3){
      count++;
    }// if statement
    
    Val1 = hand[4] % 13; 
    Val2 = hand[0] % 13; 
    Val3 = hand[2] % 13; 
    
    if (Val1 == Val2 && Val2 == Val3){
      count++;
    }// if statement
    
    //count tests for each possible combination for three in a row 
    //if count is greater than 0, then there is a three in a row hand 
 
      if (count > 0){
        gumbo = true; 
      }
      else {
        gumbo = false; 
      }// if statement
    
      return gumbo; 
    
  } // three of a kind method 
  
  public static String [] ranks (int [] hand){
    String [] ranks = new String [5];
    for (int i = 0; i < ranks.length; i++){
      
      if (hand[i] % 13 == 0){
        ranks [i] = "Ace";
      }
      if (hand[i] % 13 == 1){
        ranks [i] = "2";
      }
      if (hand[i] % 13 == 2){
        ranks [i] = "3";
      }
      if (hand[i]% 13 == 3){
        ranks [i] = "4";
      }
      if (hand[i]% 13 == 4){
        ranks [i] = "5";
      }         
      if (hand[i]% 13 == 5){
        ranks [i] = "6";
      }         
      if (hand[i]% 13 == 6){
        ranks [i] = "7";
      }         
      if (hand[i]% 13 == 7){
        ranks [i] = "8";
      }         
      if (hand[i]% 13 == 8){
        ranks [i] = "9";
      }         
      if (hand[i]% 13 == 9){
        ranks [i] = "10";
      }         
      if (hand[i]% 13 == 10){
        ranks [i] = "Jack";
      }         
      if (hand[i]% 13 == 11){
        ranks [i] = "Queen";
      }
     if (hand[i]% 13 == 12){
        ranks [i] = "King";
      }// it statements
      
    } //for loop to assign a string to each card in a player's hand
         
    return ranks; 
    
         } //rank method to assign ranks to all of the cards in the players' hands 
  
  
  public static boolean flush(int []hand){
    boolean jambalaya;
    if (hand[0] % 4 == hand[1] % 4 &&
        hand[1] % 4 == hand[2] % 4 &&
        hand[2] % 4 == hand[3] % 4 && 
        hand[3] % 4 == hand[4] % 4
       ){
      jambalaya = true;
    }
    else {
      jambalaya = false;
    }// if statement
    
    return jambalaya;
    
  } //flush method 
  
  public static boolean fullhouse(int[] hand){
    boolean bisque;
    if (
      hand[0] % 13 == hand[1] % 13 &&
      hand[1] % 13 == hand[2] % 13 &&
      hand[3] % 13 == hand [4] % 13 ||
      
      hand[0] % 13 == hand[1] % 13 &&
      hand[1] % 13 == hand[3] % 13 &&
      hand[2] % 13 == hand [4] % 13 ||
      
      hand[0] % 13 == hand[1] % 13 &&
      hand[1] % 13 == hand[4] % 13 &&
      hand[2] % 13 == hand [3] % 13 ||
      
      hand[1] % 13 == hand[2] % 13 &&
      hand[2] % 13 == hand[3] % 13 &&
      hand[0] % 13 == hand [4] % 13 ||
      
      hand[1] % 13 == hand[2] % 13 &&
      hand[2] % 13 == hand[4] % 13 &&
      hand[0] % 13 == hand [3] % 13 ||
      
      hand[2] % 13 == hand[3] % 13 &&
      hand[3] % 13 == hand[4] % 13 &&
      hand[3] % 13 == hand [4] % 13 ||
      
      hand[3] % 13 == hand[0] % 13 &&
      hand[0] % 13 == hand[2] % 13 &&
      hand[1] % 13 == hand [4] % 13 ||
      
      hand[3] % 13 == hand[0] % 13 &&
      hand[0] % 13 == hand[4] % 13 &&
      hand[1] % 13 == hand [2] % 13 ||
      
      hand[3] % 13 == hand[1] % 13 &&
      hand[1] % 13 == hand[4] % 13 &&
      hand[0] % 13 == hand [2] % 13 ||
      
      hand[4] % 13 == hand[0] % 13 &&
      hand[0] % 13 == hand[2] % 13 &&
      hand[1] % 13 == hand [3] % 13 
      ) 
    {
      bisque = true;
    }
    else {
      bisque = false;
    }// if statement
    
    return bisque;
    
  } //full house method (three of a kind and a pair)
  
  public static int highestRank(int [] hand){
    
    int[] sortedHand = new int[]{hand[0] % 13, hand[1] % 13, hand[2] % 13, hand[3] % 13, hand[4] % 13};
    Arrays.sort(sortedHand);
    int highestRank = sortedHand[sortedHand.length - 1];
    
    return highestRank;
    
  } //method to find the highest rank in a hand 
  
  
  
} //class 