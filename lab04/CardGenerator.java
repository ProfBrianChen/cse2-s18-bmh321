//Cards 1-13 represent the diamonds, 
//14-26 represent the clubs, then hearts, then spades.  
//In all suits, card identities ascend in step with the card number: 
//14 is the ace of clubs, 15 is the 2 of clubs, and 26 is the king of clubs.

public class CardGenerator {
  public static void main(String[] args){
    String suit = "";
    String type = "";
    int splitter = 13;
    int card = (int) (Math.random() * 52 + 1); // gives us a random number from 1 to 52
    if (card >= 1 && card <= 13 ){
      suit = "Diamonds";
    }
    
    else if (card >= 14 && card <= 26) {
      suit = "Clubs";
    }
    
    else if (card >= 27 && card <= 39) {
      suit = "Hearts";
    }
    
    else if (card >= 40 && card <= 52) {
      suit = "Spades";
    }
   
    if(card % splitter == 1){
      type = "Ace";
    }
    else if(card % splitter == 2){
      type = "Two";
    }
    else if(card % splitter == 3){
      type = "Three";
    }
    else if(card % splitter == 4){
      type = "Four";
    }
    else if(card % splitter == 5){
      type = "Five";
    }
    else if(card % splitter == 6){
      type = "Six";
    }
    else if(card % splitter == 7){
      type = "Seven";
    }
    else if(card % splitter == 8){
      type = "Eight";
    }
    else if(card % splitter == 9){
      type = "Nine";
    }
    else if(card % splitter == 10){
      type = "Ten";
    }
    else if(card % splitter == 11){
      type = "Jack";
    }
    else if(card % splitter == 12){
      type = "Queen";
    }
    else if(card % splitter == 0){
      type = "King";
    }
      
      
   System.out.println("Your card is the " + type + " of " + suit);
  } // end of main method
  
} // end of class