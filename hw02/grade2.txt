=============================
grade.txt
=============================
Grade: 93/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
The code compiles

B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
The code runs properly

C) How can any runtime errors be resolved?
N/A

D) What topics should the student study in order to avoid the errors they made in this homework?
You did not eliminate any fractional pennies that might get printed as extra decimals after the first two decimal values for tax and total amounts
E) Other comments:
Nice code, but you need to comment more.
