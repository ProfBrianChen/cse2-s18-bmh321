public class ArithmeticCalculations {
  
  public static void main(String[] args){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    double pantsTotal = pantsPrice * numPants;
    double shirtsTotal = shirtPrice * numShirts;
    double beltsTotal = beltCost * numBelts;
    
    double taxPants = pantsTotal * paSalesTax;
    double taxShirts = shirtsTotal * paSalesTax;
    double taxBelts = beltsTotal * paSalesTax;
    
    double preTaxTotal = pantsTotal + shirtsTotal + beltsTotal;
    double salesTaxTotal = taxBelts + taxShirts + taxPants;
    double totalPrice = preTaxTotal + salesTaxTotal;
    
    System.out.println("Cost of pants: " + pantsTotal);
    System.out.println("Cost of shirts: " + shirtsTotal);
    System.out.println("Cost of belts: " + beltsTotal);
    System.out.println("Tax on pants: " + taxPants);
    System.out.println("Tax on shirts: " + taxShirts);
    System.out.println("Tax on belts: " + taxBelts);
    System.out.println("Total before tax: " + preTaxTotal);
    System.out.println("Total amount of sales tax: " + salesTaxTotal);
    System.out.println("Total Price: " + totalPrice);

  }
}