import java.util.Random;

public class RobotCity {
  
  public static void main(String[] args){
    
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); //generating a random integer as input for the invade method 
    

    int [][] array = buildCity(); 
    
    display(array);
    
    invade(array, randomInt);
    
    display(array); 
    
    for (int i = 1; i < 6; i++){
      
      System.out.println("----------------------------------------------------------");
      System.out.println("Update " + i + ":");
      System.out.println(" ");
      update(array); 
      display(array); 
  
    } //end for loop 
    
  } //end of main method 
  
  public static int [][] buildCity(){
    
    //creating an array with of a random length from 10 to 15 
    int [][] array = new int [(int)(Math.random() * ((5) + 1) + 10)][(int)(Math.random() * ((5) + 1) + 10)];
    
    //initializing each member array with random numbers from 100 to 999 
    for (int row = 0; row < array.length; row++){
      for (int column = 0; column < array[row].length; column++){
        array[row][column] = (int)(Math.random() * ((999 - 100) + 1) + 100);
      } //end for loop 
    } //end for loop 
    
    
    return array;
 
  }// end of build city method 
  
  public static void display(int [][] array){
    
        
    for (int row = 0; row < array.length; row++){
      for (int column = 0; column < array[row].length; column++){
        System.out.printf(" %d ", array[row][column]);
      } //end for loop 
      System.out.println(); 
      System.out.println();
    } //end for loop 
       
  } //end of display method 
  
  public static int [][] invade (int [][] array, int k){
    
   for (int i = 0; i < k; i++){
     int row = (int)(Math.random() * (array.length));
     int column = (int)(Math.random() * (array[0].length));
     array[row][column] = (-1) * array[row][column];
     
   } //end for loop 
    return array;   
  } //end of invade method
  
  public static void update(int [][] array){
    
    for (int row = 0; row < array.length; row++){
      for (int column = 0; column < array[row].length; column++){
        
        if (array[row][column] < 0){
          
          array[row][column] = array[row][column] * (-1);
          
          if (column < array[row].length - 1){
            array[row][++column] = array [row][column] * (-1);
            
          } //end if statement   
        } //end if statement
      } //end for loop 
    } //end for loop 
  }
} //end of class 
