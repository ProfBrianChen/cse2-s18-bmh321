import java.util.Scanner;

public class CSE2Linear{
  public static void linearSearch(int[] array){
    Scanner linear = new Scanner(System.in);
    System.out.print("Give us a test score");
    int score = linear.nextInt();
    for(int k = 0; k < array.length; k++){
      System.out.print(array[k] + " ");
      if(score == array[k]){
        System.out.println( score + " was found!");
        break;
      }// if statement
      else{
        System.out.println(score + " was not found.");
      }// else statement
    }// for loop
  }// method
  
  public static void binarySearch(int[] array){
    Scanner binary = new Scanner(System.in);
    System.out.print("Give us a test score");
    int score = binary.nextInt();
    for(int m = 0; m < array.length; m++){
      System.out.print(array[m] + " ");
      if(score == array[m]){
        System.out.println( score + " was found!");
        break;
      }// if statement
      else{
        System.out.println(score + " was not found.");
      }// else statement
    }// for loop
    
  }// method
  
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    
    int length = 15;
    
    int[] numbers;
    numbers = new int[length];
    
    for(int i = 0; i < 15; i++){
      int val = 0;
      System.out.print("Give a number from 0-100, that is larger than any previous number you've given.");
      val = scan.nextInt();
      boolean small;
      if(scan.hasNextInt() == false){
        System.out.println("This is not an integer!");
        break;
      } // if statement
      for(int j = i; j > 0; j--){
        if(val < numbers[j]){
          small = true;
        }
      } // for loop
      if(val < 0 || val > 100){
        System.out.println("This number isn't between 0 and 100!");
        break;
      } // if statement
      else if(small = true){
        System.out.println("That number is smaller than one you've already submitted!");
        break;
      } // else if statement
      else{
        numbers[i] = val;
      } // else statement
    } // for loop
    
    linearSearch(numbers);
    binarySearch(numbers);
  } // main method
} // class