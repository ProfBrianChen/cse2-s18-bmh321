import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
}
 


public static void linear(int []arr,int find){ // Linear method
    boolean found=false;
    int it=0;
    for(int i=0; i<15&&!found; i++){
        if(arr[i]==find){
           found=true;
           it=i;
        } // if statement
    }// for loop
    if (found == true){ // if the number entered is true then give the number
            System.out.println(find+" was found in " + it + " iterations");
        }// if statement
        else
           System.out.println("Number not found. Please try another number"); // if not true enter another number
}// method


public static void scramble(int []arr){ // method scramble 
    for(int i=0; i<15; i++){
        int rand=(int)(Math.random()*15);
        int temp=arr[i];
        arr[i]=arr[rand];
        arr[rand]=temp;
    }// for loop
    String s="[";
    for(int i=0; i<15; i++){
        s+=" "+arr[i];
    }// for loop
    System.out.println(s+" ]");
} // method



public static void binary(int []arr,int find){ // method binary 
    int start = 0;
    int end = arr.length-1;
    int index = (end+start)/2;
    boolean found = false;
    int i = 0;
    while(start<=end){
        if(find>arr[index]){
            start = index+1;
        }// if statement 
        else if(arr[index]==find){
            start=end+1;
            found=true;
        } // else if statement
        else {
            end=index-1;
        } // else statement
        index = (end+start)/2;
        i++;
    }//while loop
        if (found == true){
            System.out.println(find+" was found in " + i + " iterations");
        } // if statement
        else
           System.out.println("number not found");
} // end of method